<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Image to Base64</title>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <link href="asset/dropify/css/dropify.min.css" rel="stylesheet">
    <link href="asset/vadmin/css/bootstrap.min.css" rel="stylesheet">
    <link href="asset/vadmin/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="asset/vadmin/css/plugins/timeline.css" rel="stylesheet">
    <link href="asset/vadmin/css/sb-admin-2.css" rel="stylesheet">
    <link href="asset/vadmin/css/plugins/morris.css" rel="stylesheet">
    <link href="asset/vadmin/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="asset/dropify/js/dropify.min.js"></script>
</head>
<body>

<div class="">
    <div class="container-fluid">  
    <h1 style="font-family: times, serif";><center>Image JPEG to Base64 String</center></h1> 
        <input type="file" id="pilihfile" class="dropify" onchange="encodeImageFileAsURL(this)" />
        <br>
        <b style="font-size:15px">Result Convert : </b> 
        <textarea readonly="" name="resimgtobase64" class="form-control" id="resimgtobase64" cols="30" rows="13"></textarea>
        <br><p>
        <a href="../base64gambar"><input type="button" class="btn btn-primary" name="kembali" value="Kembali"></a>
        <a href="imagetobase64.php"><input type="button" class="btn btn-danger" name="reset" value="Reset"></a>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.dropify').dropify();
    });
</script>

<script>
    function encodeImageFileAsURL(element) {
        var inputFile1 = document.getElementById('pilihfile');
        var pathFile1 = inputFile1.value;
        var ekstensiOk = /(\.jpg)$/i;
        if(!ekstensiOk.exec(pathFile1)){
            alert('Silakan upload file yang memiliki ekstensi .jpg ..!');
            inputFile1.value = '';
            return false;
        }else{
            var oFReader1 = new FileReader();
            oFReader1.readAsDataURL(document.getElementById("pilihfile").files[0]);
            var f1 = document.getElementById("pilihfile").files[0];
            var fsize1 = f1.size||f1.fileSize;
            if(fsize1 > 2000000) {
                alert("Ukuran file foto terlalu besar dari 2MB..!");
                inputFile1.value = '';
                return false;		    
            } else {
                var file = element.files[0];
                var reader = new FileReader();
                reader.onloadend = function() {
                    var tampung = document.getElementById('resimgtobase64').value = reader.result;
                    var hasil = tampung.replace('data:image/jpeg;base64,','');
                    document.getElementById('resimgtobase64').value = hasil;
                }
                reader.readAsDataURL(file);
            }    
        }
    }
</script>
</body>
</html>