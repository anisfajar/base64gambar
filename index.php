<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Convert File</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="asset/vadmin/css/bootstrap.min.css" rel="stylesheet">
    <link href="asset/vadmin/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="asset/vadmin/css/plugins/timeline.css" rel="stylesheet">
    <link href="asset/vadmin/css/sb-admin-2.css" rel="stylesheet">
    <link href="asset/vadmin/css/plugins/morris.css" rel="stylesheet">
    <link href="asset/vadmin/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>

<div class="container">
    <div class="content">
        <div class="col-md-5">
            <br><b style="font-size:18px"> Silahkan Pilih</b><br><p><br>
            <a href="base64toimage.php"><img width="220px" src="asset/images/stringtoimg.png" alt=""><br><br></a>
            <a href="base64toimage.php">
                <button type="button" class="btn btn-primary">
                    Base64 String to Image JPEG
                </button>
            </a> 
            <br><br><p><br><p>
            <a href="imagetobase64.php"><img width="220px" src="asset/images/imgtostring.png" alt=""><br><br></a>
            <a href="imagetobase64.php">
                <button type="button" class="btn btn-info">
                    Image JPEG to Base64 String
                </button>
            </a>
        </div>
    </div>
</div>
</body>
</html>