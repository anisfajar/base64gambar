<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Base64 to Image</title>
    <link href="asset/vadmin/css/bootstrap.min.css" rel="stylesheet">
    <link href="asset/vadmin/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="asset/vadmin/css/plugins/timeline.css" rel="stylesheet">
    <link href="asset/vadmin/css/sb-admin-2.css" rel="stylesheet">
    <link href="asset/vadmin/css/plugins/morris.css" rel="stylesheet">
    <link href="asset/vadmin/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="">
    <div class="container-fluid">  
    <h1 style="font-family: times, serif";><center>BASE64 String to Image JPEG</center></h1> 
        <form action="" method="post">
            <textarea cols="180" rows="10" type="text" name="base64toimage" class="form-control"></textarea><p><p>
            <input type="submit" class="btn btn-info" name="btnsubmit" value="Generate">
            <div style="margin-left:1175px;margin-top:-40px">
                <a href="base64toimage.php"><input type="button" class="btn btn-danger" name="reset" value="Reset"></a>
                <a href="../base64gambar"><input type="button" class="btn btn-primary" name="kembali" value="Kembali"></a>
            </div>
        </form>
    </div>
</div>
</body>
</html>
<?php
    if (isset($_POST['btnsubmit'])) {  
        $img_str = $_POST['base64toimage'];
        if ($img_str == "") {
            echo "<script>alert('Inputan tidak boleh kosong')</script>";
        } else {
            echo 
            '<div class="">
                <div class="container-fluid">  
                    <center><img src="data:image/jpg;base64,'.$img_str.'" /></center>
                </div>
            </div>';
        }
    }
?>